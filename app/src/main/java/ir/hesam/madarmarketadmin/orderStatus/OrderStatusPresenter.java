package ir.hesam.madarmarketadmin.orderStatus;

import java.util.ArrayList;

import ir.hesam.madarmarketadmin.module.DeliverdItems;
import ir.hesam.madarmarketadmin.orderStatus.fragment.DeliverFragment;

public class OrderStatusPresenter {

    private static OrderStatusPresenter orderStatusPresenter ;

    public static OrderStatusPresenter getInstance() {
        if (orderStatusPresenter == null){
            orderStatusPresenter = new OrderStatusPresenter();
        }
        return orderStatusPresenter;
    }

//    public void getItem_delivered(DeliverFragment fragment) {
//        ArrayList<DeliverdItems> items =new ArrayList<>();
//
//        items.add(new DeliverdItems());
//        items.add(new DeliverdItems());
//        items.add(new DeliverdItems());
//        items.add(new DeliverdItems());
//        items.add(new DeliverdItems());
//        items.add(new DeliverdItems());
//
//        fragment.adapter.versionModels.accept(items);
//    }
}
