package ir.hesam.madarmarketadmin.orderStatus.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import java.util.ArrayList;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ir.hesam.madarmarketadmin.G;
import ir.hesam.madarmarketadmin.R;
import ir.hesam.madarmarketadmin.adapter.OrderStatusAdapter;
import ir.hesam.madarmarketadmin.databinding.SendingFragmentBinding;
import ir.hesam.madarmarketadmin.module.Customer;
import ir.hesam.madarmarketadmin.netWork.NetworkLayer;

public class SendingFragment extends Fragment {

    public OrderStatusAdapter adapter;
    SendingFragmentBinding binding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.sending_fragment, container, false);
        View view = binding.getRoot();

//        adapter = new OrderStatusAdapter(this.getContext());
//
//        NetworkLayer.getInstance().basket_orders(G.getToken() , "Sendig")
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new SingleObserver<ArrayList<Customer>>() {
//                    @Override
//                    public void onSubscribe(@NonNull Disposable d) {
//
//                    }
//
//                    @Override
//                    public void onSuccess(@NonNull ArrayList<Customer> list) {
//                        binding.progress.setVisibility(View.INVISIBLE);
//                        binding.recycler.setVisibility(View.VISIBLE);
//
//                        binding.recycler.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, true));
//                        binding.recycler.setAdapter(adapter);
//                        adapter.versionModels.accept(list);
//
//                    }
//
//                    @Override
//                    public void onError(@NonNull Throwable e) {
//
//                    }
//                });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        adapter = new OrderStatusAdapter(this.getContext());

        NetworkLayer.getInstance().basket_orders(G.getToken() , "Sendig")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<ArrayList<Customer>>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onSuccess(@NonNull ArrayList<Customer> list) {
                        binding.progress.setVisibility(View.INVISIBLE);
                        binding.recycler.setVisibility(View.VISIBLE);

                        binding.recycler.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, true));
                        binding.recycler.setAdapter(adapter);
                        adapter.versionModels.accept(list);

                        if (list.isEmpty()){
                            binding.recycler.setVisibility(View.INVISIBLE);
                            binding.empty.notFound.setVisibility(View.VISIBLE);
                        }


                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                    }
                });

    }
}
