package ir.hesam.madarmarketadmin.orderStatus;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;

import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;

import ir.hesam.madarmarketadmin.R;
import ir.hesam.madarmarketadmin.databinding.ActivityOrderStatusBinding;
import ir.hesam.madarmarketadmin.orderStatus.fragment.DeliverFragment;
import ir.hesam.madarmarketadmin.orderStatus.fragment.PaidFragment;
import ir.hesam.madarmarketadmin.orderStatus.fragment.SendingFragment;

public class OrderStatusActivity extends AppCompatActivity {

    ActivityOrderStatusBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_order_status);

        FragmentPagerItemAdapter adapter = new FragmentPagerItemAdapter(
                getSupportFragmentManager(), FragmentPagerItems.with(this)
                .add(getString(R.string.paid), PaidFragment.class)
                .add(getString(R.string.sending), SendingFragment.class)
                .add(getString(R.string.delivered), DeliverFragment.class)
                .create());

        binding.viewpager.setAdapter(adapter);
        binding.viewpagertab.setViewPager(binding.viewpager);
    }
}