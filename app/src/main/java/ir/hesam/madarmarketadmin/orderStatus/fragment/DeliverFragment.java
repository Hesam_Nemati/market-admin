package ir.hesam.madarmarketadmin.orderStatus.fragment;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ir.hesam.madarmarketadmin.G;
import ir.hesam.madarmarketadmin.R;
import ir.hesam.madarmarketadmin.Utils;
import ir.hesam.madarmarketadmin.adapter.OrderStatusAdapter;
import ir.hesam.madarmarketadmin.databinding.DeliveredFragmentBinding;
import ir.hesam.madarmarketadmin.module.Customer;
import ir.hesam.madarmarketadmin.netWork.NetworkLayer;

public class DeliverFragment extends Fragment {
    DeliveredFragmentBinding binding;
    public OrderStatusAdapter adapter;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.delivered_fragment, container, false);
        View view = binding.getRoot();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        adapter = new OrderStatusAdapter(this.getContext());

        NetworkLayer.getInstance().basket_orders(G.getToken() , "Deliverd")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<ArrayList<Customer>>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onSuccess(@NonNull ArrayList<Customer> list) {
                        binding.progress.setVisibility(View.INVISIBLE);
                        binding.recycler.setVisibility(View.VISIBLE);

                        adapter.versionModels.accept(list);
                        binding.recycler.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, true));
                        binding.recycler.setAdapter(adapter);


                        if (list.isEmpty()){
                            binding.recycler.setVisibility(View.INVISIBLE);
                            binding.empty.notFound.setVisibility(View.VISIBLE);
                        }


                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        Toast.makeText(getContext(), "سبد خرید شما خالی است", Toast.LENGTH_SHORT).show();
                    }
                });

    }
}
