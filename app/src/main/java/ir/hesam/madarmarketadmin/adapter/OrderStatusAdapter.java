package ir.hesam.madarmarketadmin.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.jakewharton.rxrelay2.BehaviorRelay;

import java.io.Serializable;
import java.util.ArrayList;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import ir.hesam.madarmarketadmin.R;
import ir.hesam.madarmarketadmin.Utils;
import ir.hesam.madarmarketadmin.basketDetail.UserAndBasketDetailActivity;
import ir.hesam.madarmarketadmin.databinding.BasketDetailBaseBinding;
import ir.hesam.madarmarketadmin.module.Address_obj;
import ir.hesam.madarmarketadmin.module.Customer;
import ir.hesam.madarmarketadmin.orderStatus.fragment.DeliverFragment;
import ir.hesam.madarmarketadmin.orderStatus.fragment.PaidFragment;

public class OrderStatusAdapter extends  RecyclerView.Adapter<OrderStatusAdapter.VersionViewHolder> {

    Context context;


    public OrderStatusAdapter(Context context ) {
        this.context = context;

    }

    public BehaviorRelay<ArrayList<Customer>> versionModels = BehaviorRelay.createDefault(new ArrayList<>());

    {
        versionModels.observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ArrayList<Customer>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(ArrayList<Customer> customer) {

                        notifyDataSetChanged();
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }



    @Override
    public VersionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.basket_detail_base, parent, false);
        OrderStatusAdapter.VersionViewHolder viewHolder = new OrderStatusAdapter.VersionViewHolder(view);

        return viewHolder;
    }



    @RequiresApi(api = Build.VERSION_CODES.O)
    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(final VersionViewHolder holder, final int position) {
        //position -> current item


       holder.binding.cardView.setOnClickListener(view -> {
           Intent i = new Intent(context , UserAndBasketDetailActivity.class);
//           i.putExtra("ID", 24 );
           i.putExtra("ID", versionModels.getValue().get(position).getId() );
           i.putExtra("RESIVER" , versionModels.getValue().get(position).getUser_name());
           i.putExtra("DATE" , versionModels.getValue().get(position).getDate_time());
           i.putExtra("getAddress" , versionModels.getValue().get(position).getAddress_obj().getAddress());
           i.putExtra("getLat" , versionModels.getValue().get(position).getAddress_obj().getLat());
           i.putExtra("getLon" , versionModels.getValue().get(position).getAddress_obj().getLon());


           context.startActivity(i);
       });

       holder.binding.dateTv.setText(Utils.toJalali(versionModels.getValue().get(position).getDate_time()));
//       holder.binding.totalAmountTv.setText(versionModels.getValue().get(position).getAmount());
       holder.binding.totalAmountTv.setText(Utils.seperateThousands(versionModels.getValue().get(position).getAmount()));
       holder.binding.receiverTv.setText(versionModels.getValue().get(position).getUser_name());


        if (versionModels.getValue().get(position).getState().equals("پرداخت شده")){
            holder.binding.linearColor.setBackgroundColor(context.getResources().getColor(R.color.colorPrimaryPix));
        }
        if (versionModels.getValue().get(position).getState().equals("ارسال شد")){
            holder.binding.linearColor.setBackgroundColor(context.getResources().getColor(R.color.light_blue));
        }
        if (versionModels.getValue().get(position).getState().equals("در حال ارسال")){
            holder.binding.linearColor.setBackgroundColor(context.getResources().getColor(R.color.yellow));
        }
    }


    @Override
    public int getItemCount() {
        return versionModels.getValue() == null ? 0 : versionModels.getValue().size();
    }

    public class VersionViewHolder extends RecyclerView.ViewHolder {

        BasketDetailBaseBinding binding;

        public VersionViewHolder(View itemView) {
            super(itemView);
            binding = BasketDetailBaseBinding.bind(itemView);
        }
    }


}

