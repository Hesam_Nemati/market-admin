package ir.hesam.madarmarketadmin.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.jakewharton.rxrelay2.BehaviorRelay;

import java.util.ArrayList;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import ir.hesam.madarmarketadmin.R;
import ir.hesam.madarmarketadmin.Utils;
import ir.hesam.madarmarketadmin.basketDetail.UserAndBasketDetailActivity;
import ir.hesam.madarmarketadmin.databinding.ProductBaseBinding;
import ir.hesam.madarmarketadmin.module.ProductItems;
import ir.hesam.madarmarketadmin.module.Product_obj;
import ir.hesam.madarmarketadmin.module.products;

public class ProductsAdapter extends  RecyclerView.Adapter<ProductsAdapter.VersionViewHolder> {

    UserAndBasketDetailActivity context;


    public ProductsAdapter(UserAndBasketDetailActivity context) {
        this.context = context;

    }

    public BehaviorRelay<ArrayList<products>> versionModels = BehaviorRelay.createDefault(new ArrayList<>());

    {
        versionModels.observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ArrayList<products>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(ArrayList<products> product_objs) {

                        notifyDataSetChanged();
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }


    @Override
    public ProductsAdapter.VersionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_base, parent, false);
        ProductsAdapter.VersionViewHolder viewHolder = new ProductsAdapter.VersionViewHolder(view);

        return viewHolder;
    }



    @Override
    public void onBindViewHolder(final ProductsAdapter.VersionViewHolder holder, final int position) {
        //position -> current item

       holder.binding.productNameTv.setText(versionModels.getValue().get(position).getProduct_obj().getName());
       holder.binding.oneProductPriceTv.setText(Utils.seperateThousands(versionModels.getValue().get(position).getProduct_obj().getMain_price()));
       holder.binding.numberOfProductTv.setText(String.valueOf(versionModels.getValue().get(position).getCount()));
       holder.binding.totalProductPriceTv.setText(Utils.seperateThousands(versionModels.getValue().get(position).getSum_product_price()));

    }

    @Override
    public int getItemCount() {
        return versionModels.getValue() == null ? 0 : versionModels.getValue().size();
    }

    public class VersionViewHolder extends RecyclerView.ViewHolder {

        ProductBaseBinding binding;

        public VersionViewHolder(View itemView) {
            super(itemView);
            binding = ProductBaseBinding.bind(itemView);
        }
    }


}

