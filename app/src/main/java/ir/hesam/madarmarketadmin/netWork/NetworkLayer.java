package ir.hesam.madarmarketadmin.netWork;

import java.util.ArrayList;

import io.reactivex.Single;
import ir.hesam.madarmarketadmin.module.Customer;
import ir.hesam.madarmarketadmin.module.DeliverdResponse;
import ir.hesam.madarmarketadmin.module.OrdersItem;
import ir.hesam.madarmarketadmin.module.UserLogin;
import retrofit2.http.Field;
import retrofit2.http.Header;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public class NetworkLayer implements ApiClient {

    private ApiClient apiClient;

    private static final NetworkLayer ourInstance = new NetworkLayer();

    public static NetworkLayer getInstance() {
        return ourInstance;
    }

    public NetworkLayer() {
        apiClient = ApiService.getApiClient();
    }

    @Override
    public Single<UserLogin> login(String username, String password) {
        return apiClient.login(username, password);
    }

    @Override
    public Single<ArrayList<Customer>> basket_orders(String token , String state) {
        return apiClient.basket_orders(token , state);
    }

    @Override
    public Single<OrdersItem> basket_orders_id(String token, int id) {
        return apiClient.basket_orders_id(token, id);
    }

    @Override
    public Single<DeliverdResponse> deliverd(String token , int id , String state) {
        return apiClient.deliverd(token, id, state);
    }


}
