package ir.hesam.madarmarketadmin.netWork;

import java.util.ArrayList;

import io.reactivex.Single;
import ir.hesam.madarmarketadmin.module.Customer;
import ir.hesam.madarmarketadmin.module.DeliverdResponse;
import ir.hesam.madarmarketadmin.module.OrdersItem;
import ir.hesam.madarmarketadmin.module.UserLogin;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiClient {

    @FormUrlEncoded
    @POST("/auth/login/base/")
    Single<UserLogin> login(@Field("username") String username, @Field("password") String password);

    @GET("/basket/orders/")
    Single<ArrayList<Customer>> basket_orders(@Header("Authorization") String token , @Query("state") String state);

//    @GET("basket/order/{id}/")
    @GET("basket/order/{id}")
    Single<OrdersItem> basket_orders_id(@Header("Authorization") String token , @Path("id") int id);

    @FormUrlEncoded
    @PUT("/basket/order/{id}/")
    Single<DeliverdResponse> deliverd(@Header("Authorization") String token , @Path("id") int id , @Field("state") String state);

}
