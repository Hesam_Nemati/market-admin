package ir.hesam.madarmarketadmin.login;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import ir.hesam.madarmarketadmin.R;
import ir.hesam.madarmarketadmin.databinding.ActivityLoginBinding;
import ir.hesam.madarmarketadmin.home.HomeActivity;
import ir.hesam.madarmarketadmin.orderStatus.OrderStatusActivity;

public class LoginActivity extends AppCompatActivity {

    ActivityLoginBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        login();
    }

    // Get Username & Password For Send To Api
    public void login() {
        binding.loginBtn.setOnClickListener(view -> {

            LoginActivityPresenter.getInstance().login_userName(this,
                    binding.userNameEt.getText().toString(),
                    binding.passWordEt.getText().toString());
        });
    }
}