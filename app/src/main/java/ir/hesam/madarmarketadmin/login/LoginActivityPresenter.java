package ir.hesam.madarmarketadmin.login;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ir.hesam.madarmarketadmin.G;
import ir.hesam.madarmarketadmin.home.HomeActivity;
import ir.hesam.madarmarketadmin.module.UserLogin;
import ir.hesam.madarmarketadmin.netWork.NetworkLayer;

public class LoginActivityPresenter {


    private static final LoginActivityPresenter ourInstance = new LoginActivityPresenter();
    public static LoginActivityPresenter getInstance(){ return ourInstance;}

    public LoginActivityPresenter() {
    }

    public void login_userName(LoginActivity activity , String userName , String passWord) {
        NetworkLayer.getInstance().login(userName, passWord)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<UserLogin>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onSuccess(@NonNull UserLogin userLogin) {
                        activity.binding.loginBtn.setVisibility(View.INVISIBLE);
                        activity.binding.progressBtn.setVisibility(View.VISIBLE);


                        G.add_to_sharedPref(userLogin.getAccess()); // <-- Save Token In SharePref
                        Intent i = new Intent(activity, HomeActivity.class);
                        activity.startActivity(i);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
//                        Toast.makeText(activity, "از درستی نام کاربری و رمز عبور دقت حاصل فرمایید", Toast.LENGTH_SHORT).show();
                        activity.binding.errorTv.setVisibility(View.VISIBLE);
                        activity.binding.errorTv.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                activity.binding.errorTv.setVisibility(View.INVISIBLE);
                            }
                        },3000);
                    }
                });
    }
}
