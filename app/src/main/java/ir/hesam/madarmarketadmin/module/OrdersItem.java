package ir.hesam.madarmarketadmin.module;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class OrdersItem {


    @SerializedName("result")
    BasketResult basketResult = new BasketResult();

    public BasketResult getBasketResult() {
        return basketResult;
    }
}
