package ir.hesam.madarmarketadmin.module;

import com.google.gson.annotations.SerializedName;

public class Address_obj {

    @SerializedName("id")
    private int id;

    @SerializedName("name")
    private String AddressName;

    @SerializedName("lat")
    private double lat;

    @SerializedName("lon")
    private double lon;

    @SerializedName("address")
    private String address;

    @SerializedName("customer")
    private int customer;

    public int getId() {
        return id;
    }

    public String getAddressName() {
        return AddressName;
    }

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }

    public String getAddress() {
        return address;
    }

    public int getCustomer() {
        return customer;
    }

    @Override
    public String toString() {
        return "Address_obj{" +
                "id=" + id +
                ", AddressName='" + AddressName + '\'' +
                ", lat=" + lat +
                ", lon=" + lon +
                ", address='" + address + '\'' +
                ", customer=" + customer +
                '}';
    }
}
