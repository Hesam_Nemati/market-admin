package ir.hesam.madarmarketadmin.module;

import com.google.gson.annotations.SerializedName;

public class products {

    @SerializedName("product_obj")
    Product_obj product_obj =new Product_obj();

    @SerializedName("count")
    private int count;

    @SerializedName("total_discount_price")
    private String total_discount_price;

    public Product_obj getProduct_obj() {
        return product_obj;
    }

    public int getCount() {
        return count;
    }



    @SerializedName("sum_product_price")
    private String sum_product_price;

    public String getSum_product_price() {
        return sum_product_price;
    }

    public String getTotal_discount_price() {
        return total_discount_price;
    }

    @Override
    public String toString() {
        return "products{" +
                "product_obj=" + product_obj +
                ", count=" + count +
                ", total_discount_price='" + total_discount_price + '\'' +
                '}';
    }
}
