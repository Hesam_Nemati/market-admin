package ir.hesam.madarmarketadmin.module;

import com.google.gson.annotations.SerializedName;

public class Customer {


    @SerializedName("id")
    private int id;

    @SerializedName("user_id")
    private int user_id;

    @SerializedName("address_obj")
    Address_obj address_obj = new Address_obj();

    @SerializedName("user_name")
    private String user_name;

    @SerializedName("state")
    private String state;

    @SerializedName("is_paid")
    private boolean is_paid;

    @SerializedName("amount")
    private String amount;

    @SerializedName("date_time")
    private String date_time;

    @SerializedName("discount_code_amount")
    private String discount_code_amount;

    @SerializedName("basket")
    private String basket;

    @SerializedName("address")
    private String address;

//    @SerializedName("detail")
//    private String detail;
//
//    public String getDetail() {
//        return "سبد خرید شما خالی است";
//    }

    public int getId() {
        return id;
    }

    public int getUser_id() {
        return user_id;
    }

    public Address_obj getAddress_obj() {
        return address_obj;
    }

    public String getUser_name() {
        return user_name;
    }

    public String getState() {

        if (state.equals("NotPaid"))
            return  "پرداخت نشده";
        if (state.equals("Paid"))
            return  "پرداخت شده";
        if (state.equals("Deliverd"))
            return  "ارسال شد";
        if (state.equals("Sendig"))
            return  "در حال ارسال";

        return state;
    }

    public boolean isIs_paid() {
        return is_paid;
    }

    public String getAmount() {
        return amount;
    }

    public String getDate_time() {
        return date_time;
    }

    public String getDiscount_code_amount() {
        return discount_code_amount;
    }

    public String getBasket() {
        return basket;
    }

    public String getAddress() {
        return address;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", user_id=" + user_id +
                ", address_obj=" + address_obj +
                ", user_name='" + user_name + '\'' +
                ", state='" + state + '\'' +
                ", is_paid=" + is_paid +
                ", amount='" + amount + '\'' +
                ", date_time='" + date_time + '\'' +
                ", discount_code_amount='" + discount_code_amount + '\'' +
                ", basket='" + basket + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
