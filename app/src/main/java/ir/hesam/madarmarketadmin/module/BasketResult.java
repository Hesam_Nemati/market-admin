package ir.hesam.madarmarketadmin.module;

import android.icu.text.UFormat;
import android.text.Html;
import android.text.Spanned;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Timer;

import timber.log.Timber;

public class BasketResult {


    static String tag = "<span ";
    static String c_tag = "</span>";

    static String del_tag="<del ";
    static String c_del_tag = "</del>";

    static String tab_b = "<b>";
    static String c_tag_b = "</b>";


    @SerializedName("state")
    private String state;

    @SerializedName("description")
    private String description;

    @SerializedName("is_online")
    private String is_online;

    public String isIs_online() {
        if (is_online.equals("true"))
            return "آنلاین پرداخت شده است";
        if (is_online.equals("false"))
            return "پرداخت درب منزل";

        return is_online;
    }

    @SerializedName("user_mobile_phone")
    private String user_mobile_phone;

    @SerializedName("basket")
    private int basket;

    @SerializedName("products")
    ArrayList<products> products_list = new ArrayList<products>();

    @SerializedName("is_paid")
    private boolean is_paid;

    @SerializedName("amount")
    private String amount;

    @SerializedName("address")
    private int address;

    @SerializedName("delivery_price")
    private int delivery_price;

    public int getDelivery_price() {
        return delivery_price;
    }

    public int getCountTotal(){
        int a = 0 ;

        for (int i = 0 ; i < getProducts_list().size() ; i++){

            a = a +(getProducts_list().get(i).getCount());

        }

        return a;
    }

    @SerializedName("discount_code_amount")
    private String discount_code_amount;

    public String getState() {

        if (state.equals("NotPaid"))
            return "پرداخت نشده" ;
        if (state.equals("Paid"))
            return "پرداخت شده";
        if (state.equals("Deliverd"))
            return "ارسال شد";
        if (state.equals("Sendig"))
            return "در حال ارسال";

        return state;
    }

    public String getDescription() {
        return description;
    }

    public int getBasket() {
        return basket;
    }

    public ArrayList<products> getProducts_list() {
        return products_list;
    }

    public boolean isIs_paid() {
        return is_paid;
    }

    public String getAmount() {
        return amount;
    }

    public int getAddress() {
        return address;
    }

    public String getUser_mobile_phone() {
        return user_mobile_phone;
    }

    public String getDiscount_code_amount() {
        return discount_code_amount;
    }

    public Spanned styled(String text){
         return Html.fromHtml(tag+"style='color:#c9184a;'>"+text+c_tag);
    }

    public static String getTag() {
        return tag;
    }
}
