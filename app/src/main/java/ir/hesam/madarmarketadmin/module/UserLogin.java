package ir.hesam.madarmarketadmin.module;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UserLogin implements Serializable {


    @SerializedName("username")
    private String username;

    @SerializedName("first_name")
    private String first_name;

    @SerializedName("last_name")
    private String last_name;

    @SerializedName("id")
    private String id;


    @SerializedName("access")
    private String access;

    @SerializedName("refresh")
    private String refresh;

    public String getUsername() {
        return username;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getId() {
        return id;
    }

    public String getAccess() {
        return access;
    }

    public String getRefresh() {
        return refresh;
    }

    @Override
    public String toString() {
        return "UserLogin{" +
                "username='" + username + '\'' +
                ", first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", id='" + id + '\'' +
                ", access='" + access + '\'' +
                ", refresh='" + refresh + '\'' +
                '}';
    }
}
