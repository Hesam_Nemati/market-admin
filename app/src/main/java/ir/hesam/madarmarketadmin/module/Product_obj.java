package ir.hesam.madarmarketadmin.module;

import com.google.gson.annotations.SerializedName;

public class Product_obj {

    @SerializedName("name")
     private String name;

    @SerializedName("main_price")
    private String main_price;

    public String getName() {
        return name;
    }

    public String getMain_price() {
        return main_price;
    }

    @Override
    public String toString() {
        return "Product_obj{" +
                "name='" + name + '\'' +
                ", main_price='" + main_price + '\'' +
                '}';
    }
}
