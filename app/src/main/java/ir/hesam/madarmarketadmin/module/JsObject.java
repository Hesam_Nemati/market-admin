package ir.hesam.madarmarketadmin.module;

import android.webkit.JavascriptInterface;

import androidx.annotation.NonNull;

public class JsObject {

    @NonNull
    @JavascriptInterface
    @Override
    public String toString() {
        return "injectedObject";
    }

}
