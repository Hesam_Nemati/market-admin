package ir.hesam.madarmarketadmin.basketDetail;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ir.hesam.madarmarketadmin.G;
import ir.hesam.madarmarketadmin.Utils;
import ir.hesam.madarmarketadmin.adapter.ProductsAdapter;
import ir.hesam.madarmarketadmin.databinding.ActivityUserAndBasketDetailBinding;
import ir.hesam.madarmarketadmin.module.Customer;
import ir.hesam.madarmarketadmin.module.DeliverdResponse;
import ir.hesam.madarmarketadmin.module.OrdersItem;
import ir.hesam.madarmarketadmin.netWork.NetworkLayer;

public class UserAndBasketDetailActivity extends AppCompatActivity {

    ActivityUserAndBasketDetailBinding binding;
    ProductsAdapter adapter;
    Customer customer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityUserAndBasketDetailBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        adapter = new ProductsAdapter(this);


        Map<String , String> changeStatus = new HashMap<>();
        changeStatus.put("delivering" , "delivered");


        Map<String,String> changeBtnStatus = new HashMap<>();
        changeBtnStatus.put("Paid" , "پرداخت شده");
        changeBtnStatus.put("Deliverd" , "ارسال شده");
        changeBtnStatus.put("sendig" , "درحال ارسال");


        //region Get Extra
        // Initialize Variable
        int id;
        String reciver, date, address;
        double lat, lon;

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                id = Integer.parseInt(null);
                lat = Double.parseDouble(null);
                lon = Double.parseDouble(null);
                reciver = null;
                date = null;
                address = null;

            } else {
                id = extras.getInt("ID");
                reciver = extras.getString("RESIVER");
                date = extras.getString("DATE");
                lat = extras.getDouble("getLat");
                lon = extras.getDouble("getLon");
                address = extras.getString("getAddress");
                customer = (Customer) extras.get("single_custome");

            }
        } else {
            id = (Integer) savedInstanceState.getSerializable("ID");
            reciver = (String) savedInstanceState.getSerializable("RESIVER");
            date = (String) savedInstanceState.getSerializable("DATE");
            lat = (Double) savedInstanceState.getSerializable("getLat");
            lon = (Double) savedInstanceState.getSerializable("getLon");
            address = (String) savedInstanceState.getSerializable("getAddress");

        }


        //endregion


        // SetLayoutManager For Adapter
        binding.productRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));


        NetworkLayer.getInstance().basket_orders_id(G.getToken(), id)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new SingleObserver<OrdersItem>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @RequiresApi(api = Build.VERSION_CODES.O)
                    @Override
                    public void onSuccess(@NonNull OrdersItem ordersItem) {
                        binding.progress.setVisibility(View.INVISIBLE);
                        binding.mainRl.setVisibility(View.VISIBLE);

                        adapter.versionModels.accept(ordersItem.getBasketResult().getProducts_list());
                        binding.productRecycler.setAdapter(adapter);

                        binding.numberOfItemsTv.setText(String.valueOf(ordersItem.getBasketResult().getCountTotal()));

                        binding.receiverTv.setText(reciver);
                        binding.phoneNumberTv.setText(ordersItem.getBasketResult().getUser_mobile_phone());
                        binding.totalAmountAfterDiscountTv.setText(Utils.seperateThousands(ordersItem.getBasketResult().getAmount()));
                        binding.dateTv.setText(Utils.toJalali(date));
                        binding.paymentMethodTv.setText(ordersItem.getBasketResult().styled(ordersItem.getBasketResult().getState()));
                        binding.addressTv.setText(address);
                        binding.orderCodeTv.setText(String.valueOf(id));
                        binding.descriptionTv.setText(ordersItem.getBasketResult().getDescription());
                        binding.deliveringAmountTv.setText(Utils.seperateThousands(String.valueOf(ordersItem.getBasketResult().getDelivery_price())));

                        binding.statusTv.setText(ordersItem.getBasketResult().isIs_online());

                        if (ordersItem.getBasketResult().getDescription() == null){
                            binding.descriptionTv.setText("------");
                        }

                        binding.addressTv.setOnClickListener(view -> {
                            String uri = String.format(Locale.ENGLISH, "geo:%f,%f", lon, lat);
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                            startActivity(intent);
                        });

                        binding.phoneNumberTv.setOnClickListener(view -> {
                            Intent i=new Intent(Intent.ACTION_DIAL,Uri.parse("tel:" + ordersItem.getBasketResult().getUser_mobile_phone()));
                            UserAndBasketDetailActivity.this.startActivity(i);
                        });

                        if  (ordersItem.getBasketResult().getProducts_list() == null){
                            binding.ll.setVisibility(View.INVISIBLE) ;
                            binding.nullRes.rltv11.setVisibility(View.VISIBLE);
                        }

                        binding.deliverdBtn.setText(ordersItem.getBasketResult().getState());
                        if (ordersItem.getBasketResult().getState().equals("پرداخت شده")){
                            binding.deliverdBtn.setEnabled(true);
                            binding.deliverdBtn.setText("پرداخت شده -> (تغییر وضعیت به در حال ارسال)");
                            binding.deliverdBtn.setOnClickListener(view -> {

                                    NetworkLayer.getInstance().deliverd(G.getToken() , id , "Sendig")
                                            .subscribeOn(Schedulers.io())
                                            .observeOn(AndroidSchedulers.mainThread())
                                            .subscribe(new SingleObserver<DeliverdResponse>() {
                                                @Override
                                                public void onSubscribe(@NonNull Disposable d) {

                                                }

                                                @Override
                                                public void onSuccess(@NonNull DeliverdResponse deliverdResponse) {
                                                    binding.deliverdBtn.setVisibility(View.INVISIBLE);
                                                    binding.bottomProgress.setVisibility(View.VISIBLE);
                                                    Toast.makeText(UserAndBasketDetailActivity.this , "این محصول به (درحال ارسال) تغییر وضعیت داد", Toast.LENGTH_SHORT).show();
                                                    onBackPressed();

                                                }

                                                @Override
                                                public void onError(@NonNull Throwable e) {

                                                }
                                            });
                                });

                        }
                        if (ordersItem.getBasketResult().getState().equals("ارسال شد")){
                            binding.deliverdBtn.setEnabled(false);

                        }
                        if (ordersItem.getBasketResult().getState().equals("در حال ارسال")){
                            binding.deliverdBtn.setEnabled(true);
                            binding.deliverdBtn.setText("تحویل گردید");
                            binding.deliverdBtn.setOnClickListener(view -> {
                                NetworkLayer.getInstance().deliverd(G.getToken() , id , "Deliverd")
                                        .subscribeOn(Schedulers.io())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(new SingleObserver<DeliverdResponse>() {
                                            @Override
                                            public void onSubscribe(@NonNull Disposable d) {

                                            }

                                            @Override
                                            public void onSuccess(@NonNull DeliverdResponse deliverdResponse) {
                                                binding.deliverdBtn.setVisibility(View.INVISIBLE);
                                                binding.bottomProgress.setVisibility(View.VISIBLE);
                                                Toast.makeText(UserAndBasketDetailActivity.this , "تحویل گردید", Toast.LENGTH_SHORT).show();
                                                onBackPressed();

                                            }

                                            @Override
                                            public void onError(@NonNull Throwable e) {

                                            }
                                        });
                            });
                        }


                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                        binding.ll.setVisibility(View.INVISIBLE) ;
                        binding.nullRes.rltv11.setVisibility(View.VISIBLE);

                    }
                });





    }


}