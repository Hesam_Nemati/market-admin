package ir.hesam.madarmarketadmin.basketDetail;

import android.content.Intent;
import android.net.Uri;

import java.util.ArrayList;
import java.util.Locale;

import ir.hesam.madarmarketadmin.adapter.ProductsAdapter;
import ir.hesam.madarmarketadmin.module.DeliverdItems;
import ir.hesam.madarmarketadmin.module.ProductItems;
import ir.hesam.madarmarketadmin.orderStatus.OrderStatusPresenter;
import ir.hesam.madarmarketadmin.orderStatus.fragment.DeliverFragment;

public class UserAndBasketDetailPresenter {

    public static UserAndBasketDetailPresenter userAndBasketDetailPresenter;

    public static UserAndBasketDetailPresenter getInstance() {

        if (userAndBasketDetailPresenter == null){
            userAndBasketDetailPresenter = new UserAndBasketDetailPresenter();
        }
        return userAndBasketDetailPresenter;
    }




//    public void gotoAddress(UserAndBasketDetailActivity activity){
//
//        activity.binding.addressTv.setOnClickListener(view -> {
//            String uri = String.format(Locale.ENGLISH, "geo:%f,%f", 43.56, 53.2);
//            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
//            activity.startActivity(intent);
//
//
//
//
//        });
//    }

//    public void getAddItem(UserAndBasketDetailActivity activity) {
//        ArrayList<ProductItems> items =new ArrayList<>();
//
//        items.add(new ProductItems());
//        items.add(new ProductItems());
//        items.add(new ProductItems());
//        items.add(new ProductItems());
//        items.add(new ProductItems());
//        items.add(new ProductItems());
//
//        activity.adapter.versionModels.accept(items);
//    }
}
