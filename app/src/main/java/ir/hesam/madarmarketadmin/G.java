package ir.hesam.madarmarketadmin;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.multidex.MultiDexApplication;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import timber.log.Timber;

public class G extends MultiDexApplication {
    /***
     * For Save Pref Should be Initialize In SplashActivity
     */

    public static Context context;
    private static SharedPreferences sharedPref;
    private static String token;
    public static Context getContext() {
        return context;
    }



    public static Context getGetContentResolvercontext() {
        return getContentResolvercontext;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Timber.plant(new Timber.DebugTree());

        context = getApplicationContext();
    }

    public static String getToken() {  // <-- This Method is NEVER Be Null
        getFromPref();
        return "Bearer "+token;
    }
    public static String getPureToken() { // <-- This Method Can Be Null    &
        //  We Should Use This Method For Save Token For Login In Splash Activity
        getFromPref();
        return token;
    }

    public static void getFromPref() {
        if (context == null) {
        }
        sharedPref = context.getSharedPreferences("Hamiss", context.MODE_PRIVATE); // <-- Make SharePref (DataBase = Memory) whit "Hammis"
        token = sharedPref.getString("TOKEN", null);

    }

    public static void add_to_sharedPref(String token ){

        sharedPref = context.getSharedPreferences("Hamiss", Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("TOKEN", token);
        editor.apply();

    }
    public static Context getContentResolvercontext;



    public static SharedPreferences getSharedPref(){return sharedPref;}

}