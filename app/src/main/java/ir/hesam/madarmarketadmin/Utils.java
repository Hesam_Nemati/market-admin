package ir.hesam.madarmarketadmin;

import android.os.Build;

import androidx.annotation.RequiresApi;

import com.github.mfathi91.time.PersianDate;
import com.github.mfathi91.time.PersianMonth;

import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;

import timber.log.Timber;

public class Utils {

    private static Utils instance;
//    private static PersianDate persianDate;

    public static Utils getInstance() {
        if (instance == null) {
            instance = new Utils();
        }
        return instance;
    }

//    public static String toJalali(String date) {
//        String myGdate = "";
////        String myTime = "";
//        if (date.contains(":")) {
//            myGdate = date.split(" ")[0];
////            myTime = date.split(" ")[1];
////        } else {
////            myGdate = date;
////        }
//
//            String[] details = myGdate.split("/");
//
//
//            int year = Integer.parseInt(details[0]);
//            int month = Integer.parseInt(details[1]);
//            int day = Integer.parseInt(details[2]);
//
//            PersianDate persianDate = new PersianDate();
//
//
//            int[] dd = persianDate.gregorian_to_jalali(year, month, day);
//            return dd[0] + "/" + dd[1] + "/" + dd[2] + " ";
//
//        }
//    }

    @RequiresApi(api = Build.VERSION_CODES.O)

    public static String toJalali(String date) {


//        date = "2022/03/05";


        String[] date1 = date.split("/");

//        Calendar beginTime = Calendar.getInstance();
//        beginTime.set(Integer.parseInt(date1[2]), Integer.parseInt(date1[1]), Integer.parseInt(date1[0]), Integer.parseInt(time1[0]), Integer.parseInt(time2[0]));

        int year = Integer.parseInt(date1[0]);
        int mounth = Integer.parseInt(date1[1]);
        int day = Integer.parseInt(date1[2]) ;


        PersianDate persianDate6 = PersianDate.fromGregorian(LocalDate.of(year , mounth , day));

        String pDate = String.valueOf(persianDate6);

        return pDate.replace("-","/");

    }

    public static String amount_int(){


        return null;
    }

    public static String seperateThousands(String amount){

        String myString = amount ;
        double toman = Double.parseDouble(myString);


        DecimalFormat decimalFormat = new DecimalFormat("#");
        decimalFormat.setGroupingUsed(true);
        decimalFormat.setGroupingSize(3);

        return  decimalFormat.format(toman) ;
    }
}