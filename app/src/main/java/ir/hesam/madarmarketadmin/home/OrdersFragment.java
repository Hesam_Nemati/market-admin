package ir.hesam.madarmarketadmin.home;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;

import ir.hesam.madarmarketadmin.R;
import ir.hesam.madarmarketadmin.databinding.OrdersFragmentBinding;
import ir.hesam.madarmarketadmin.orderStatus.OrderStatusActivity;
import ir.hesam.madarmarketadmin.orderStatus.fragment.DeliverFragment;
import ir.hesam.madarmarketadmin.orderStatus.fragment.PaidFragment;
import ir.hesam.madarmarketadmin.orderStatus.fragment.SendingFragment;

public class OrdersFragment extends Fragment {

    private static final OrdersFragment ourInstance = new OrdersFragment();

    public static OrdersFragment getInstance() {
        return ourInstance;
    }

    OrdersFragmentBinding binding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.orders_fragment, container, false);
        View view = binding.getRoot();

        /***
         * Here We Should Set Smart "Tab Layout"
         * Add Fragment
         * Set String Values
         * ViewPager & SmartTabLayout Adapter
         */
        FragmentPagerItemAdapter adapter = new FragmentPagerItemAdapter(
                getChildFragmentManager(), FragmentPagerItems.with(getContext())
                .add(getString(R.string.delivered), DeliverFragment.class)
                .add(getString(R.string.sending), SendingFragment.class)
                .add(getString(R.string.paid), PaidFragment.class)
                .create());

        binding.viewpager.setAdapter(adapter);
        binding.viewpagertab.setViewPager(binding.viewpager);

        return view;
    }
}
