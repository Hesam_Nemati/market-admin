package ir.hesam.madarmarketadmin.home;

import static android.content.ContentValues.TAG;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import ir.hesam.madarmarketadmin.R;
import ir.hesam.madarmarketadmin.databinding.ActivityHomeBinding;

public class HomeActivity extends AppCompatActivity {

    ActivityHomeBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_home);

        getFragment(OrdersFragment.getInstance()); // <-- First Time Be Here , Before Set

        // Set BottomNavigation
        binding.bottomNavigation.setOnNavigationItemSelectedListener(item -> {


            if (item.getItemId() == R.id.orders) {
                Toast.makeText(HomeActivity.this, "سفارش ها", Toast.LENGTH_SHORT).show();
                //getFragment(new UploadFragment());
                getFragment(OrdersFragment.getInstance());
                Log.e(TAG, "onCreate: " + "OrdersFragment");

            } else if (item.getItemId() == R.id.special_order) {
                Toast.makeText(HomeActivity.this, "پنل ادمین", Toast.LENGTH_SHORT).show();
                //getFragment(new HomeFragment());
                getFragment(SpecialOrdersFragment.getInstance());
                Log.e(TAG, "onCreate: " + "SpecialOrdersFragment");

            }

            return true;

        });
    }

// In XML ResourceFile We Should Set Relative & set BottomNavigation Below Of Frame Layout
    private void getFragment(Fragment fragment) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = manager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_layout, fragment);
        fragmentTransaction.commit();
    }

}