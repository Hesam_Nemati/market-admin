package ir.hesam.madarmarketadmin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import ir.hesam.madarmarketadmin.home.HomeActivity;
import ir.hesam.madarmarketadmin.login.LoginActivity;
import ir.hesam.madarmarketadmin.orderStatus.OrderStatusActivity;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_and_basket_detail);

        if (G.getPureToken() == null){
            Intent i = new Intent(this, LoginActivity.class);
            this.startActivity(i);
        }
        else
        {
            Intent i = new Intent(this, HomeActivity.class);
            this.startActivity(i);
        }


    }
}